import requests
from os import environ, path
from dotenv import load_dotenv
import time

# Load environment variables from .env file
basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))

def scraper():
    
    

    # Configuration
    web_server_url = "http://backend:8000" # change depending on the server
    admin_login_url = f"{web_server_url}/auth/login"
    update_feeds_url = f"{web_server_url}/scraper/update_all_feeds_articles"
    admin_credentials = {
        'username': environ.get('ADMIN_USERNAME'),
        'password': environ.get('ADMIN_PASSWORD')
    }

    # Authenticate as admin

    auth_request = requests.post(admin_login_url, json=admin_credentials)
    if auth_request.status_code == 200:
        access_token = auth_request.json()['access_token']
        headers = {'Authorization': f'Bearer {access_token}'}

        # Call the protected route
        update_request = requests.get(update_feeds_url, headers=headers)
        print("Update Feeds Response:", update_request.text)
    else:
        print("Failed to authenticate as admin.")
    return 

if __name__ == "__main__":
    while True:
        try :
            scraper()
            print("ok")
        except Exception as e:
            print(e)
            pass
        time.sleep(5)